import requests
import csv
import os
from threading import Lock

counter = 0

class LoadTest:
  def __init__(self, url, filename):
    self.url = url
    set_headers = True

    self.lock = Lock()
    if not os.path.isfile(filename) or os.stat(filename).st_size == 0:
      set_headers = False
    with open(filename, 'a+', newline='') as csvfile:
      fieldnames = ["StatusCode", "ResponseTime"]
      if not set_headers:
        self.writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
        self.writer.writeheader()
      self.file = csvfile

  def insertRow(self, row):
    if self.writer:
      self.writer.writerow(row)

  def testUrl(self, filename):
    global counter
    with open(filename, "a+", newline='') as self.file:
      self.lock.acquire()
      writer = csv.writer(self.file)
      response = requests.get(self.url)
      counter+=1
      print("respo code : {0} , counter : {1}".format(response.status_code , counter))
      writer.writerow([response.status_code, response.elapsed.total_seconds()])
      self.lock.release()
