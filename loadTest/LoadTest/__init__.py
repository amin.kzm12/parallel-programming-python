from LoadTest.loadTest import LoadTest
import schedule
from threading import  Thread

fileName = "report.csv"
adminTest = LoadTest("https://admin.cyberdesign.info", fileName)
def run():
  thread_lst = [Thread(target=adminTest.testUrl, args=(fileName,)) for i in range(100)]
  [thread.start() for thread in thread_lst]
  [thread.join() for thread in thread_lst]

if __name__ == '__main__':
  # run()
  schedule.every(5).seconds.do(run)
  while 1:
    schedule.run_pending()



