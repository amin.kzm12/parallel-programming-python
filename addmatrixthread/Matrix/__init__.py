from threading import Thread
import time

mat1 = [[1, 0, 1, 0],
        [0, 1, 1, 0],
        [1, 0, 1, 0],
        [0, 1, 1, 0]]

mat2 = [[0, 2, 2, 0],
        [2, 0, 2, 0],
        [0, 2, 2, 0],
        [2, 0, 2, 0]]

thread_num = 4
matResult = [[0 for i in range(len(mat1))] for j in range(len(mat2))]


def add_matrix(flag,step):
  global mat1, mat2, matResult, thread_num
  # matResult = [[mat1[i][j] + mat2[i][j] for j in range(len(mat2))] for i in range(flag,len(mat1),thread_num)]
  for i in range(flag, step + flag,1):
    for j in range(len(mat2)):
      matResult[i][j] = mat1[i][j] + mat2[i][j]


if __name__ == '__main__':
  step = int(thread_num / len(mat1))
  step = (1/step) if step < 1 else step
  start = time.time()
  thread_lst = [Thread(target=add_matrix, args=(flag,step)) for flag in range(0, thread_num , step)]
  [thread.start() for thread in thread_lst]
  [thread.join() for thread in thread_lst]
  end = time.time()

  print(matResult)
  print(end-start)
